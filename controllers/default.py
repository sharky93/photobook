# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a samples controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################

@auth.requires_login()
def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html
    """
    response.flash = "Welcome to Photobook!"
    myself = auth.user.id
    records = db(db.auth_user.id!=auth.user.id).select(db.auth_user.ALL, orderby=db.auth_user.id)
    form = SQLFORM(db.image)
    form.vars.image_id = myself
    if form.process().accepted:
    	response.flash = 'Pic Uploaded'
    images = db(db.image.image_id == myself).select(db.image.ALL, orderby=db.image.title)
    return dict(images = images,uploadform = form, message='%(first_name)s'%auth.user, records = records)

def show():
    print request.args(0),request.args(1)
    if(request.args(1)):
    	currentuserimage = db((db.image.image_id==request.args(1))&(db.image.id!=request.args(0))).select(db.image.ALL, orderby=db.image.title)
   	image = db(db.image.id==request.args(0)).select().first() #for selected image
    	name = db(db.auth_user.id==request.args(1)).select().first() #for selected image
    else:
   	image = db(db.image.image_id==request.args(0)).select().first() #for selected image
    	currentuserimage = db((db.image.image_id==request.args(0))).select(db.image.ALL, orderby=db.image.title)
    	name = db(db.auth_user.id==request.args(0)).select().first() #for selected image
    flag = 0
    print "flag is", flag
    print image
    print currentuserimage
    if not image:
	    flag = 1
    elif not currentuserimage and not request.args(1):
	    flag = 1
    if flag == 0:
    	db.image.file.readable = False
    	db.image.file.writable = False
    	record = db(db.image.image_id == image.id).select(db.image.image_id)
    	record2 = db.image(image.id)			# FOR DELETING IMAGE
	form_del = SQLFORM(db.image,record2,deletable="true")
    	form_del.custom.submit.attributes['_value']="Update/Delete Image"
    if flag == 0:
   	 db.comment.image_id.default = image.id

    form = SQLFORM(db.comment)
    form.custom.submit.attributes['_value']="Comment"
    form.vars.author = auth.user.first_name + " " + auth.user.last_name
    if flag == 0:
    	likes_mine = db((db.like.liker == auth.user.id)&(db.like.image_id == image.id)).select(db.like.ALL)
    	likes = db(db.like.image_id == image.id).select(db.like.ALL)
   	print likes
    db.like.image_id.readable = False
    db.like.liker.readable = False
    form_like = SQLFORM(db.like)
    form_like.vars.name = auth.user.first_name
    form_like.custom.submit.attributes['_value']="Like"
    if flag == 0:
    	db.like.image_id.default = image.id
    	db.like.liker.default = auth.user.id
   	print image.id, auth.user.id, "Print"
    form_like_notme = ""
    if flag == 0:
    	if likes:
		print "somebody likes this"
    		if likes_mine:
    			 likes_all = db(db.like.image_id == image.id).select(db.like.ALL)
   			 form_like = "You"
			 for anything in likes_all:
				 if anything.liker == auth.user.id:		## LIKES
					 continue
				 form_like = form_like + ', '
				 form_like = form_like + anything.name
        		 form_like = form_like + ' like this'
    			 #record_dislike = db(db.like.image_id == image.id).select()			# FOR DELETING IMAGE
			 print "image id is",image.id 
    			 record_dislike = db.like(db.like.image_id == image.id)			# FOR DELETING IMAGE
    			 db.like.image_id.readable = False
    			 db.like.image_id.writable = False
    			 db.like.liker.readable = False
			 form_dislike = SQLFORM(db.like,record_dislike,deletable="true")
    			 form_dislike.custom.submit.attributes['_value']="DisLike"
    			 if form_dislike.process().accepted:
				    url = "index"
				    redirect(URL(url))
				    response.flash = 'you dont like this'
    		else:	
			 for anything in likes:
				 form_like_notme = form_like_notme + ', ' 			## LIKES
				 form_like_notme = form_like_notme + anything.name
        		 form_like_notme = form_like_notme + ' likes this'
    			 if form_like.process().accepted:
				    url = "index"
				    redirect(URL(url))
				    response.flash = 'you like this'
    	else:	
		 for anything in likes:
			 form_like_notme = form_like_notme + ', ' 			## LIKES
			 form_like_notme = form_like_notme + anything.name
        	 form_like_notme = form_like_notme
    		 if form_like.process().accepted:
		    url = "index"
		    redirect(URL(url))
		    response.flash = 'you like this'
    	if record2.image_id == auth.user.id:
   		if form_del.process().accepted:
		           response.flash = 'Image Deleted!'
			   redirect(URL('index'))
    	else:
			form_del = "-------------"
    
    if form.process().accepted:
	    response.flash = 'your comment is posted'
    myself = auth.user.id
    print myself,"Flag is",flag
    if flag == 0:
    	comments = db(db.comment.image_id==image.id).select()
    #return dict(image=image, comments=comments, form=form)
    	images = db(db.image.image_id == myself).select(db.image.ALL, orderby=db.image.title)  #for user logged in
    records = db(db.auth_user.id!=myself).select(db.auth_user.ALL, orderby=db.auth_user.id)
    if flag == 1:
    	return dict(name = name,image = image, images = "null",comments = "null",currentuserimage = currentuserimage, form = "null",records = records,form_like = "null", form_like_notme = "null", form_dislike = "null")
    if type(form_like) is str:
    	return dict(form_del = form_del,name = name, records = records,image = image, images = images, currentuserimage = currentuserimage, comments = comments, form = form, form_like = form_like, form_dislike = form_dislike, form_like_notme = form_like_notme)

    return dict(form_del = form_del,name = name, records = records,image = image, images = images, currentuserimage = currentuserimage, comments = comments, form = form, form_like = form_like, form_dislike = "null",form_like_notme = form_like_notme)
@auth.requires_membership('manager')
def makeadmin():
	grid2 = SQLFORM.smartgrid(db.auth_membership)
	return dict(grid2 = grid2)
@auth.requires_membership('manager')
def admin():
	grid = SQLFORM.smartgrid(db.image)
	return dict(grid = grid)
def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request,db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())
def register():
	form = SQLFORM(db.person)
	if form.process().accepted:
		response.flash = 'Registration Successful'
	records = SQLTABLE(db().select(db.person.ALL),headers='fieldname:capitalize')
	return dict(form=form, records=records)
